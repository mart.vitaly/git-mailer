const express = require('express')
const { body } = require('express-validator/check')

const router = express.Router()

const notificationController = require('../controllers/notification.controller')
const authMiddleware = require('../middlewares/auth.middleware')
const validationMiddleware = require('../middlewares/validation.middleware')

/**
 * @typedef NotificationModel
 * @property {string} username.required - GitHub username of user
 * @property {string} message - message for email
 */

router.use(authMiddleware.checkToken)

/**
 * This function is using to send emaik
 * @route POST notification/send_email
 * @group Notification - operation with notifications
 * @param {NotificationModel.model} body.body.required - body
 * @returns 200 - User info found by email and password
 * @security JWT
 */

router.post('/send_email', [
  body('username').exists().not().isEmpty(),
  body('message').exists()
], validationMiddleware, notificationController.sendEmail)

module.exports = router
