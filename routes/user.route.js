const express = require('express')
const crypto = require('crypto')
const mime = require('mime')
const multer = require('multer')
const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, './tmp/')
  },
  filename: (req, file, cb) => {
    crypto.pseudoRandomBytes(16, (err, raw) => {
      cb(null, raw.toString('hex') + Date.now() + '.' + mime.getExtension(file.mimetype))
    })
  }
})
const upload = multer({ storage: storage })
const { body } = require('express-validator/check')

const router = express.Router()

const userController = require('../controllers/user.controller')
const authMiddleware = require('../middlewares/auth.middleware')
const validationMiddleware = require('../middlewares/validation.middleware')

/**
 * @typedef UserCreate
 * @property {string} email.required - email of user
 * @property {string} avatar - file
 * @property {string} password.required - user password
 */

/**
 * @typedef GetUser
 * @property {string} email.required - email of user
 * @property {string} password - user password
 */

/**
 * This function is using to create user
 * @route POST /users/registration
 * @group Users - Operations with users
 * @param {UserCreate.model} body.body.required - body
 * @returns 200 - link to avatar and token
 */

router.post('/registration', upload.single('avatar'), [
  body('email').exists().isEmail().not().isEmpty(),
  body('password').exists()
], validationMiddleware, userController.createUser)

/**
 * This function is using to get user
 * @route POST /users/login
 * @group Users - Operations with users
 * @param {GetUser.model} body.body.required - body
 * @returns 200 - User info found by email and password
 */
router.post('/login', [
  body('email').exists().isEmail().not().isEmpty(),
  body('password').exists()
], validationMiddleware, userController.login)

router.use(authMiddleware.checkToken)

/**
 * This function is using to get avatar
 * @route GET /users/avatar/{fileName}
 * @param {string} fileName.path.required - avatar file name
 * @group Users - Operations with users
 * @returns 200 - return avatar
 * @security JWT
 */
router.get('/avatar/:fileName', userController.getAvatar)

module.exports = router
