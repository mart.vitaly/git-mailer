const bodyParser = require('body-parser')
const express = require('express')
const expressip = require('express-ip')
const mongoose = require('mongoose')
const config = require('./config')
const AppError = require('./libs/AppError')

const app = express()

app.use((req, res, next) => {
  res.header('Access-Control-Allow-Origin', '*')
  res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept')
  next()
})

mongoose.connect(config.server.mongoURI, {
  useNewUrlParser: true
})
app.use(expressip().getIpInfoMiddleware)
mongoose.connection.on('error', (error) => {
  if (error) {
    throw new AppError('DB_CONNECTION_ERROR')
  }
})
mongoose.connection.once('open', () => {})
const expressSwagger = require('express-swagger-generator')(app)

app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true }))

app.use('/api/users', require('./routes/user.route'))
app.use('/api/notification', require('./routes/notification.route'))

expressSwagger(config.swagger)

app.use(AppError.handle404)
app.use(AppError.handler)

app.listen(config.server.port, () => {
  console.log(`listening on port ${config.server.port}`)
})
