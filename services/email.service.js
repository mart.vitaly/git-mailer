const Mailgen = require('mailgen')
const weather = require('weather-js')
const AppError = require('../libs/AppError')
const config = require('../config')
const mailgun = require('mailgun-js')({ apiKey: config.mailgun.apiKey, domain: config.mailgun.domain })

const m = {}

m.send = async (email, message, ip) => {
  const mailGenerator = new Mailgen({
    theme: 'default',
    product: {
      name: 'Test task',
      link: 'https://test.js/'
    }
  })
  const weatherString = await getCurrentWeather(ip)
  const emailOptions = {
    body: {
      intro: message,
      signature: weatherString
    }
  }
  const emailHtml = mailGenerator.generate(emailOptions)
  const data = {
    from: `Test task <${config.mailgun.sender}>`,
    to: email,
    subject: 'Test message',
    html: emailHtml
  }

  return mailgun.messages()
    .send(data)
    .then((body) => {
      return true
    }).catch((error) => {
      if (error) {
        throw new AppError('EMAIL_SEND')
      }
    })
}

async function getCurrentWeather (ipInfo) {
  return new Promise((resolve, reject) => {
    weather.find({ search: `${ipInfo.city}, ${ipInfo.country}`, degreeType: 'C' }, (err, result) => {
      if (err) {
        reject(err)
      }
      const currentWeather = result[0].current
      return resolve(
        `Location: ${currentWeather.observationpoint}<br> 
        Date: ${currentWeather.date},<br> 
        Temperature: ${currentWeather.temperature} C,<br> 
        Sky: ${currentWeather.skytext},<br>
        Windspeed: ${currentWeather.windspeed}
        `)
    })
  })
}

module.exports = m
