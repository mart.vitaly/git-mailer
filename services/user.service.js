const bcrypt = require('bcrypt')
const AppError = require('../libs/AppError')
const User = require('../models/user.model')
const config = require('../config')
const m = {}

m.createUser = async (data) => {
  data.password = await hashPassword(data.password)
  return User.create(data)
}

m.getUserByEmail = async (data) => {
  const user = await User.findOne({ email: data.email })
  if (!user) {
    throw new AppError('USER_NOT_FOUND')
  }
  return user
}

m.comparePassword = async (hashPassword, passoword) => {
  return bcrypt.compare(passoword, hashPassword)
}

module.exports = m

function hashPassword (passoword) {
  return new Promise((resolve, reject) => {
    bcrypt.genSalt(config.server.salt, (err, salt) => {
      if (err) {
        throw new AppError('PASSWORD_HASH')
      }
      bcrypt.hash(passoword, salt, (err, hash) => {
        if (err) {
          throw new AppError('PASSWORD_HASH')
        }
        return resolve(hash)
      })
    })
  })
}
