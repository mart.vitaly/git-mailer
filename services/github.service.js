const request = require('superagent')
const config = require('../config')
const m = {}

m.getGitHubUserEmail = async (username) => {
  const res = await request
    .get(`https://api.github.com/users/${username}`)
    .set('Authorization', `token ${config.github.token}`)
    .set('X-Oauth-Scope', 'user:email')
    .set('accept', 'json')
  return res.body.email
}

module.exports = m
