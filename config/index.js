module.exports = {
  server: {
    port: process.env.PORT || 3000,
    mongoURI: process.env.MONGODB_URI || 'mongodb://localhost:27017/test-task',
    salt: 10
  },
  github: {
    token: ''
  },
  mailgun: {
    apiKey: '',
    domain: '',
    sender: ''
  },
  jwt: {
    expiration: 1000 * 60 * 60 * 24,
    jwtSecret: 'sssssssssssssssssss1',
    version: 1
  },
  swagger: {
    swaggerDefinition: {
      info: {
        description: 'This is a test task API documentation',
        title: 'Test task',
        version: '1.0.0'
      },
      host: 'test-task-test.herokuapp.com/api',
      basePath: '/',
      consumes: [
        'application/json'
      ],
      produces: [
        'application/json'
      ],
      schemes: ['https'],
      securityDefinitions: {
        JWT: {
          type: 'apiKey',
          in: 'header',
          name: 'Authorization',
          description: ''
        }
      }
    },
    basedir: __dirname,
    files: ['../routes/*.js']
  }
}
