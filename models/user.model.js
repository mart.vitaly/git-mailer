const mongoose = require('mongoose')
const Schema = mongoose.Schema

const userShema = new Schema({
  email: String,
  password: String,
  avatar: String
})

const User = mongoose.model('User', userShema)

module.exports = User
