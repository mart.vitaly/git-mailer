'use strict'

const jwt = require('jsonwebtoken')

const config = require('../../config')

const m = Object.create(null)

m.generateToken = (data = null) => {
  return jwt.sign(data, config.jwt.jwtSecret, { expiresIn: config.jwt.expiration ? parseInt(config.jwt.expiration) : 60 * 60 * 24 * 30 })
}

m.verification = (token) => {
  try {
    return jwt.verify(token, config.jwt.jwtSecret)
  } catch (error) {
    return null
  }
}

module.exports = m
