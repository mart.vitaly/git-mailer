module.exports = {
  'NOT_FOUND': {
    name: 'Not Found',
    message: '404 NOT Found',
    status: 404
  },
  'USER_NOT_FOUND': {
    name: 'User not found',
    message: 'User not found',
    status: 404
  },
  'TOKEN_GENERATION': {
    name: 'Token generation',
    message: 'Error during token generation',
    status: 500
  },
  'PASSWORD_INVALID': {
    name: 'Invalid admin password',
    message: 'Admin password is invalid',
    status: 401
  },
  'AUTH_TOKEN_NOT_FOUND': {
    name: 'Authentication token not found',
    message: 'Authorization token not found or empty',
    status: 404
  },
  'AUTH_TOKEN_INVALID': {
    name: 'Invalid Authentication token',
    message: 'Authentication token invalid',
    status: 401
  },
  'RESOURCE_NOT_FOUND': {
    name: 'ResourceNotFound',
    message: 'Resource not found',
    status: 404
  },
  'VALIDATION_ERROR': {
    name: 'ValidationError',
    message: 'Validation Error',
    status: 400
  },
  'LOCATION_NOT_FOUND': {
    name: 'Not found location',
    message: 'Location by ip not found',
    status: 404
  },
  'EMAIL_SEND': {
    name: 'Send email error',
    message: 'Error while email sending',
    status: 500
  }
}
