const gitHubService = require('../services/github.service')
const emailService = require('../services/email.service')

const m = {}

m.sendEmail = async (req, res, next) => {
  try {
    const { username, message } = req.body
    const email = await gitHubService.getGitHubUserEmail(username)
    await emailService.send(email, message, req.ipInfo)
    res.json({
      resilt: true,
      message: `Email was sended to: ${email}`
    })
  } catch (error) {
    next(error)
  }
}

module.exports = m
