const thumb = require('node-thumbnail').thumb
const fs = require('fs')
const path = require('path')
const AppError = require('../libs/AppError')
const userService = require('../services/user.service')
const jwt = require('../libs/JWT')
const config = require('../config')

const m = Object.create(null)

m.createUser = async (req, res, next) => {
  try {
    const { email, password } = req.body
    const fileOptions = req.file

    thumb({
      source: fileOptions.path,
      destination: 'uploads/',
      width: 100,
      suffix: '',
      concurrency: 4
    }, async (files, err, stdout, stderr) => {
      await userService.createUser({
        email,
        password,
        avatar: fileOptions.filename
      })
      fs.unlinkSync(`tmp/${fileOptions.filename}`)
      const token = jwt.generateToken({ version: config.jwt.version })
      if (!token) {
        throw new AppError('TOKEN_GENERATION')
      }

      res.json({
        avatarUrl: `https://test-task-test.herokuapp.com/api/users/avatar/${fileOptions.filename}`,
        token: token
      })
    })
  } catch (err) {
    next(err)
  }
}

m.login = async (req, res, next) => {
  try {
    const { email, password } = req.body
    const user = await userService.getUserByEmail({ email })
    const resultPasswordCompare = await userService.comparePassword(user.password, password)
    if (!resultPasswordCompare) {
      throw new AppError('PASSWORD_INVALID')
    }
    res.json(user)
  } catch (err) {
    next(err)
  }
}

m.getAvatar = async (req, res, next) => {
  try {
    const fileName = req.params.fileName
    if (!fs.existsSync(`uploads/${fileName}`)) {
      throw new AppError('NOT_FOUND')
    }
    res.sendFile(path.resolve(`uploads/${fileName}`))
  } catch (error) {
    next(error)
  }
}

module.exports = m
