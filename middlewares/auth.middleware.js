'use strict'

const config = require('../config')
const AppError = require('../libs/AppError')
const jwt = require('../libs/JWT')

const m = Object.create(null)

m.checkToken = (req, res, next) => {
  try {
    const token = req.get('Authorization')
    if (!token) {
      throw new AppError('AUTH_TOKEN_NOT_FOUND')
    } else {
      const result = jwt.verification(token)
      if (!result || result.version !== config.jwt.version) {
        throw new AppError('AUTH_TOKEN_INVALID')
      }
      next()
    }
  } catch (error) {
    next(error)
  }
}

module.exports = m
