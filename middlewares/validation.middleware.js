const { validationResult } = require('express-validator/check')

const AppError = require('../libs/AppError')

module.exports = (req, res, next) => {
  const errorFormatter = ({ location, msg, param, value, nestedErrors }) => `${location}.${param} = ${value} - ${msg}`
  const result = validationResult(req).formatWith(errorFormatter)
  if (result.isEmpty()) {
    return next()
  }
  const messages = result.array()
  const error = new AppError('VALIDATION_ERROR', {
    params: { firstMessage: messages[0] },
    errorMessages: messages
  })
  next(error)
}
